#include <vxWorks.h>
#include <stdio.h>

int helloLoop=1;

void helloMain()
{
    printf("Set helloLoop != 1 to break out\n");

    while (helloLoop == 1)
    {
        printf("helloMain world\n");
        taskDelay(sysClkRateGet()*5);
    }
}
